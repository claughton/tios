TIOS : The internet of things for MD Simulations.
=================================================

**Tios** is being developed to support research into alternative paradigms for simulation science - particularly molecular dynamics (MD).

In the spirit of
the *Internet of Things* (https://en.wikipedia.org/wiki/Internet_of_things), **tios** turns conventional MD simulations into streaming web applications. Data production and data capture are decoupled: data collectors can gather what they want from wherever they like, whenever they like. Simulation science becomes more akin to field work than autopsy.

See: *Meletiou et al., J. Chem. Inf. Model. 2019, 59, 8, 3359–3364* (https://pubs.acs.org/doi/10.1021/acs.jcim.9b00351)

For installation and use details, please see the wiki pages.

Charlie Laughton, 15/7/2021