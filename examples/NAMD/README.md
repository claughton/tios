Example NAMD simulation with *tios*.

Note: you must have namd installed to run this job, and the command 'namd2'in your path.

To run the example:

tios-run -t 'My Tios simulation of ubiquitin' namd2 ubq_wb.conf
