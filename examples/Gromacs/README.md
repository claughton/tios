Gromacs portable run file for a similation of BPTI. 

Note: to run this job you must have Gromacs installed (but not versions 2016.1 to 2016.3) and the command 'gmx' in your path. To run, use the command:

tios-run -t "My BPTI simulation" gmx mdrun -deffnm bpti
