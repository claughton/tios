#!/usr/bin/env python
'''
Restart a Tios job on an EC2 instance.
'''
from tios import communication
import argparse
import boto3
import base64

default_ami_name = 'xbow-15-1-19'
default_instance_type = 'p2.xlarge'
default_max_price = '0.5'

parser = argparse.ArgumentParser(description='restart a tios simulation on an AWS EC2 instance')
parser.add_argument('id', type=str, help='ID of tios simulation')
parser.add_argument('--ami_name', '-a', type=str, default=default_ami_name, help='Name of the AMI to use (default={})'.format(default_ami_name))
parser.add_argument('--instance_type', '-i', type=str, default=default_instance_type, help='Instance type to use (default={})'.format(default_instance_type))
parser.add_argument('--max_price', '-m', type=str, default=default_max_price, help='Maximum spot price ($/hr) (default={})'.format(default_max_price))
args = parser.parse_args()

try:
    te = communication.TiosAgent(args.id)
except ValueError:
    print('Error - no such Tios id')
    exit(1)

status = te.status
if status == 'Running':
    print('Error - this simulation is already running')
    exit(1)

client = boto3.client('ec2')
ec2 = boto3.resource('ec2')

# Required functions:
def key_pair_exists(KeyPairName):
    '''
    Returns True if the given Key Pair name exists.
    '''
    exists = False
    response = client.describe_key_pairs()
    KeyNames = [k.get('KeyName') for k in response.get('KeyPairs', [])]
    exists = KeyPairName in KeyNames
    return exists

def security_group_exists(SecurityGroupName):
    '''
    Returns True if the given Security Group exists.
    '''
    response = client.describe_security_groups()
    GroupNames = [r['GroupName'] for r in response['SecurityGroups']]
    return SecurityGroupName in GroupNames

def instance_exists_with_security_group(GroupName):
    '''
    Returns True if there is an existing instance with the given Security Group.
    '''
    response = client.describe_instances(
        Filters=[
            {
                'Name': 'instance.group-name',
                'Values': [
                     GroupName,
                ]
            },
        ],)
    exists = len(response['Reservations']) > 0
    return exists

def image_id_from_name(ImageName):
    '''
    Returns the id of the FIRST image found that matches the given image name.
    '''
    response = client.describe_images(
        Filters=[
            {
                'Name': 'name',
                'Values': [
                    ImageName,
                ]
            },
        ]
    )
    images = response['Images']
    if len(images) == 0:
        image_id = None
    else:
        image_id = images[0].get('ImageId')
    return image_id


# The tios job will be run using a specific Security Group and Key Pair name:
security_group_name = 'TiosSecurityGroup{}'.format(args.id)
key_pair_name = 'TiosKeyPair{}'.format(args.id)

# Check 1: Check the job is not running already, by looking for an instance with the target Security Group:
if instance_exists_with_security_group(security_group_name):
    raise RuntimeError('Error - there appears to be an instance for this tios job already')
# Check 2: check the AMI name is valid:
image_id = image_id_from_name(args.ami_name)
if image_id is None:
    raise ValueError('Error: there is no AMI available with the name "{}"'.format(args.ami_name))

# Step 1: If required, create a security group for this Tios id:
if not security_group_exists(security_group_name):
    response = client.create_security_group(
        Description='Security Group for Tios job {}'.format(args.id),
        GroupName=security_group_name
    )
    security_group_id = response['GroupId']
    # Configure the Security Group:
    security_group = ec2.SecurityGroup(security_group_id)
    response = security_group.authorize_ingress(
        IpPermissions=[
            {
                'FromPort': 22,
                'IpProtocol': 'tcp',
                'IpRanges': [
                    {
                        'CidrIp': '0.0.0.0/0',
                    },
                ],
                'Ipv6Ranges': [],
                'PrefixListIds': [],
                'ToPort': 22,
                'UserIdGroupPairs': []
            },
        ],
    )
    '''
    response = security_group.authorize_egress(
        IpPermissions=[
            {
                'IpProtocol': '-1',
                'IpRanges': [
                    {
                        'CidrIp': '0.0.0.0/0'
                    },
                ],
                'Ipv6Ranges': [],
                'PrefixListIds': [],
                'UserIdGroupPairs': []
            },
        ]
    )
    '''
# Step 2: If required, create a Key Pair for this Tios id:
if not key_pair_exists(key_pair_name):
    response = client.create_key_pair(KeyName=key_pair_name)

# Step 3: Launch the instance
user_data = '''Content-Type: multipart/mixed; boundary="//"
MIME-Version: 1.0

--//
Content-Type: text/cloud-config; charset="us-ascii"
MIME-Version: 1.0
Content-Transfer-Encoding: 7bit
Content-Disposition: attachment; filename="cloud-config.txt"

#cloud-config
cloud_final_modules:
- [scripts-user, always]

--//
Content-Type: text/x-shellscript; charset="us-ascii"
MIME-Version: 1.0
Content-Transfer-Encoding: 7bit
Content-Disposition: attachment; filename="userdata.txt"

#!/bin/bash
sudo -Hb -u ubuntu -i tios-restart -r 10 -f {}
--//'''.format(args.id)

response = client.request_spot_instances(
    DryRun=False,
    ClientToken='TiosClientToken{}'.format(args.id),
    InstanceCount=1,
    LaunchSpecification={
        'SecurityGroups': [
            security_group_name,
        ],
        'ImageId': image_id_from_name(args.ami_name),
        'InstanceType': args.instance_type,
        'KeyName': key_pair_name,
        'UserData': base64.b64encode(bytes(user_data, 'utf-8')).decode(),
    },
    SpotPrice=args.max_price,
    Type='persistent'
)
print(response)
